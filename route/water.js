var expresss = require('express');
var router = expresss.Router();
var db = require('../api/db');
var bodyParser = require('body-parser');

const groupUtil = require("../controller/group_controller");
const sht20Util = require("../controller/sht20_controller");
const cameraUtil = require("../controller/camera_controller");
const solinoidUtil = require('../controller/warter_controller');
const validateUtil = require('../controller/validate_controller');
const userUtil = require('../controller/user_controller');
const { route } = require('./camera');



router.get('/time/:id', ///ส่งข้อมูลให้ device
    solinoidUtil.send_time(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            result: req.result,
            length: req.result.length
        });
        next();
    });


router.post('/solenoid_on_off', //ส่งค่า device_time ขึ้นดาด้า
    solinoidUtil.solenoid_on_off(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message : "อัพเดทสำเร็จ"
            
        });
    });

// router.get('/time_to_app/:id',    //get เวลาให้แอพ
//     solinoidUtil.send_time_setup_to_app(),
//     (req, res, next) => {
//         res.status(200).json({
//             'success': true,
//             result: res.result
//         });
//         next();
//     });



// router.get('/temp_humid_setup/:id',
//     sht20Util.send_temp_humid_to_db(),
//     (req, res, next) => {
//         res.status(200).json({
//             'success': true,
//             result: res.result
//         });
//     });


// router.post('/setup_time', //ส่งค่า device_time ขึ้นดาด้า
//     solinoidUtil.send_time_setup_to_db(),
//     (req, res, next) => {
//         res.status(200).json({
//             'success': true,
//             result: req.result
//         });
//     });




module.exports = router;