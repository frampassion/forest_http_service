var expresss = require('express');
var router = expresss.Router();
var db = require('../api/db');
var bodyParser = require('body-parser');

const groupUtil = require("../controller/group_controller");
const sht20Util = require("../controller/sht20_controller");
const cameraUtil = require("../controller/camera_controller");
const solinoidUtil = require('../controller/warter_controller');
const validateUtil = require('../controller/validate_controller');
const userUtil = require('../controller/user_controller');

router.get('/group_data',
    validateUtil.validate_token_user(),
    groupUtil.get_group_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            result: res.result
        });
        next();
    });


router.post('/group_data',
    validateUtil.validate_token_user(),
    groupUtil.insert_group_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'บันทึกข้อมูลสำเร็จ'
        });
        next();
    });


router.put('/group_data',
    validateUtil.validate_token_user(),
    groupUtil.update_group_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'แก้ไขข้อมูลสำเร็จ'
        });
        next();
    });

router.delete('/group_data',
    validateUtil.validate_token_user(),
    groupUtil.delete_group_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'ลบข้อมูลสำเร็จ'
        });
        next();
    });








module.exports = router;