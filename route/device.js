var expresss = require('express');
var router = expresss.Router();
var db = require('../api/db');
var bodyParser = require('body-parser');

const groupUtil = require("../controller/group_controller");
const sht20Util = require("../controller/sht20_controller");
const cameraUtil = require("../controller/camera_controller");
const solinoidUtil = require('../controller/warter_controller');
const validateUtil = require('../controller/validate_controller');
const userUtil = require('../controller/user_controller');
const deviceUtil = require('../controller/device_controller');

router.get('/device_type',    //get device type
    validateUtil.validate_token_user(),
    deviceUtil.get_device_type(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            result: res.result
        });
        next();
    });

router.get('/devices_data_admin/:type',                     //admin
    deviceUtil.select_devices_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            result: res.result
        });
    });

router.post('/devices_data_admin',                         // admin
    // validateUtil.validate_token_user(),
    deviceUtil.insert_devices_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'บันทึกข้อมูลสำเร็จ'
        });
    });

router.delete('/devices_data_admin',                    //admin 
    deviceUtil.delete_devices_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'ลบข้อมูลสำเร็จ'
        });
    });

router.put('/devices_data_clear_admin',                //admin
    // validateUtil.validate_token_user(),
    deviceUtil.clear_devices_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'ลบข้อมูลสำเร็จ'
        });
    });


router.put('/devices_data_admin',                  //admin
    // validateUtil.validate_token_user(),
    deviceUtil.update_devices_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'แก้ไขข้อมูลสำเร็จ'
        });
    });

router.post('/add_devices_admin',  //admin
    deviceUtil.add_device_admin(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'เพิ่มอุปกรณ์สำเร็จ'
        });
    });





router.get('/device_data/:group_id',
    validateUtil.validate_token_user(),
    deviceUtil.select_device_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            result: res.result
        });
    });


router.put('/device_data',
    validateUtil.validate_token_user(),
    deviceUtil.update_device_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'แก้ไขข้อมูลสำเร็จ'
        });
    });


router.put('/device_data_clear',
    validateUtil.validate_token_user(),
    deviceUtil.delete_device_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'ลบข้อมูลสำเร็จ'
        });
    });

router.post('/add_device',
    validateUtil.validate_token_user(),
    deviceUtil.add_device(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'เพิ่มอุปกรณ์สำเร็จ'
        });
    });

router.get('/device_type/:type',
    deviceUtil.select_device_type(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            result: res.result
        });
    });

router.put('/device_activity_data',
    validateUtil.validate_token_user(),
    // validateUtil.validate_device_activity_data(),
    deviceUtil.update_device_activity(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'บันทึกข้อมูลสำเร็จ'
        });

    });

router.post('/setup_time', //ส่งค่า device_time ขึ้นดาด้า
    validateUtil.validate_token_user(),
    deviceUtil.send_time_setup_to_db(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            result: req.result
        });
    });

router.get('/time_to_app/:id',    //get เวลาให้แอพ
    validateUtil.validate_token_user(),
    deviceUtil.send_time_setup_to_app(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            result: res.result
        });
        next();
    });


    















module.exports = router;