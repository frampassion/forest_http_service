var expresss = require('express');
var router = expresss.Router();
var db = require('../api/db');
var bodyParser = require('body-parser');

const groupUtil = require("../controller/group_controller");
const sht20Util = require("../controller/sht20_controller");
const cameraUtil = require("../controller/camera_controller");
const solinoidUtil = require('../controller/warter_controller');
const validateUtil = require('../controller/validate_controller');
const userUtil = require('../controller/user_controller');
const { route } = require('./camera');
const { validate } = require('node-cron');




router.get('/user_data',
    validateUtil.validate_token_user(),
    userUtil.user_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            result: res.result
        });
    });



router.post('/user_register',
    validateUtil.validate_user_register(),
    userUtil.user_register(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'บันทึกข้อมูลสำเร็จ'
        });
    });



router.post('/user_login',
    validateUtil.validate_user_login(),
    userUtil.User_Login(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            token: req.token,
            message: 'เข้าสู่ระบบสำเร็จ'
        });
    });



router.post('/facebook_login',
    userUtil.facebook_login(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            token: req.token,
            message: "เข้าสู่ระบบสำเร็จ"
        });
    });



router.post('/update_facebook_id',
    validateUtil.validate_token_user(),
    userUtil.update_facebook_id(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: "เพิ่มบัญชี facebook สำเร็จ"
        });
    });



router.post('/user_update_password',
    validateUtil.validate_user_update_password(),
    validateUtil.validate_user_login(),
    userUtil.user_update_password(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: "แก้ไขรหัสผ่านสำเร็จ"
        });
    });



router.put('/edit_user_data',
    validateUtil.validate_token_user(),
    // validateUtil.validate_edit_data(),
    userUtil.edit_user_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'แก้ไขข้อมูลสำเร็จ'
        });
    });

router.put('/users_data',  //admin
    // validateUtil.validate_edit_data(),
    userUtil.edit_users_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'แก้ไขข้อมูลสำเร็จ'
        });
    });

router.get('/users_data',  //admin
    userUtil.get_user_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            result: res.result

        });
    });

router.delete('/user_delete/:id',
    validateUtil.validate_token_user(),
    userUtil.delete_data(),
    (req, res, next) => {
        res.status(200).json({
            'success': true,
            message: 'ลบข้อมูลสำเร็จ'
        });
    });




module.exports = router;