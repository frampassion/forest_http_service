var bcrypt = require('bcryptjs');
var mqtt = require('mqtt');
var io = require('socket.io-client');
const { json } = require('body-parser');
var moment = require('moment');
var db = require('../api/db');
var server = require("../const/constance");
const e = require('express');
var time_zone = moment().utc(+7).format('DD/MM/YYY HH: mm: ss');
// var canstance = require('../const/constance');
const solinoid = require('./warter_controller');
const errorMessage = require('../const/error_message');

exports.get_device_type = () => {
    return ((req, res, next) => {
        db.query(`SELECT DISTINCT device_type FROM device_information`, (err, result) => {
            if (err) throw err
            else {
                res.result = result
                next();
            }
        })
    })
}


exports.select_devices_data = () => {   //admin
    return ((req, res, next) => {
        // console.log(req.user_id)
        db.query(`SELECT * FROM device_information WHERE device_type = ?`, req.params.type, (err, Selectresult) => {
            if (err) throw err
            else {
                res.result = Selectresult;
            }
            next();
        });
    });
}

exports.insert_devices_data = () => {   //admin
    return ((req, res, next) => {
        // let client = req.body.client_id
        // console.log(req.body);
        db.query(`SELECT * FROM device_information WHERE device_information.client_id = ?`, req.body.client_id, (err, Selectresult) => {
            if (err) throw err
            else {

                if (Selectresult[0]) {
                    res.status(200).json(errorMessage.err_add_device_already)
                    next();
                }
                else {
                    console.log("false");
                    let data = [
                        null,
                        null,
                        null,
                        req.body.device_type,
                        req.body.client_id,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                    ]

                    console.log(data);
                    db.query(`INSERT INTO device_information (user_id,device_id,device_name,device_type,client_id,device_critical,device_time,device_connection,device_value,device_action,caption,group_id ) VALUE (?)`, [data], (err, Insertresult) => {
                        if (err) throw err
                        next();
                    });
                }
            }
        });
    });
}




exports.update_devices_data = () => {  //admin
    return ((req, res, next) => {
        db.query(`UPDATE device_information SET device_information.device_name ='${req.body.device_name}' WHERE device_information.device_id ='${req.body.device_id}'`, (err, Updateresult) => {
            if (err) throw err
            next();
        });
    });
}

exports.delete_devices_data = () => { //admin
    return ((req, res, next) => {
        console.log(req.body)
        let client_id = req.body.client_id

        db.query(`DELETE FROM device_information WHERE client_id = ?`, client_id, (err, result) => {
            if (err) throw err
            next();
        });
    });
}

exports.clear_devices_data = () => {    //admin
    return ((req, res, next) => {

        console.log(req.body)
        db.query(`UPDATE device_information SET device_information.user_id = null, device_information.device_name = null, device_information.device_critical = null, device_information.group_id = null WHERE device_information.client_id = '${req.body.client_id}'`, (err, Deleteruslt) => {
            if (err) throw err
            next();
        });
    });
}

exports.select_device_data = () => {
    return ((req, res, next) => {
        console.log(req.user_id)
        db.query(`SELECT * FROM device_information WHERE device_information.user_id = ? AND device_information.group_id = ?`, [req.user_id, req.params.group_id], (err, Selectresult) => {

            if (err) throw err
            else {
                res.result = Selectresult;
            }
            next();
        });
    });
}


exports.update_device_data = () => {
    return ((req, res, next) => {
        // console.log(req.body)
        db.query(`UPDATE device_information SET device_information.device_name ='${req.body.device_name}' WHERE device_information.device_id ='${req.body.device_id}'`, (err, Updateresult) => {
            if (err) throw err
            next();
        });
    });
}


exports.delete_device_data = () => {
    return ((req, res, next) => {

        console.log(req.body)
        db.query(`UPDATE device_information SET device_information.user_id = null, device_information.device_name = null, device_information.device_critical = null, device_information.group_id = null WHERE device_information.client_id = '${req.body.client_id}'`, (err, Deleteruslt) => {
            if (err) throw err
            next();
        });
    });
}

exports.add_device = () => {
    return ((req, res, next) => {
        console.log(req.body)

        db.query(`SELECT * FROM device_information WHERE device_information.client_id = ?`, req.body.client_id, (err, Selectresult) => {
            if (err) throw err
            else {
                if (Selectresult[0]) {
                    Selectresult.map((element, index) => {

                        if (element.client_id === req.body.client_id) {

                            if (element.user_id != null) {
                                res.status(200).json(errorMessage.err_device_already)
                                next();
                            }
                            else {
                                // console.log("false")
                                db.query(`UPDATE device_information SET device_information.user_id ='${req.user_id}',device_information.device_name = '${req.body.device_name}',device_information.group_id = '${req.body.group_id}' WHERE device_information.client_id = '${req.body.client_id}'`, (err, result) => {
                                    if (err) throw err
                                    next()
                                });
                            }
                        }


                    })
                }
                else {
                    res.status(200).json(errorMessage.device_not_found)
                    next();
                }

            }
        });
    });
}

exports.add_device_admin = () => {  // admin
    return ((req, res, next) => {
        console.log(req.body)

        db.query(`SELECT * FROM device_information WHERE device_information.client_id = ?`, req.body.client_id, (err, Selectresult) => {
            if (err) throw err
            else {
                if (Selectresult[0]) {
                    Selectresult.map((element, index) => {

                        if (element.client_id === req.body.client_id) {
                            console.log('test');
                            if (element.client_id != null) {
                                res.status(200).json(errorMessage.err_device_already)
                                next();
                            }
                            else {
                                // console.log("false")
                                db.query(`UPDATE device_information SET device_information.user_id ='${req.user_id}',device_information.device_name = '${req.body.device_name}',device_information.group_id = '${req.body.group_id}' WHERE device_information.client_id = '${req.body.client_id}'`, (err, result) => {
                                    if (err) throw err
                                    next()
                                });
                            }
                        }


                    })
                }
                else {
                    res.status(200).json(errorMessage.device_not_found)
                    next();
                }

            }
        });
    });
}


exports.select_device_type = () => {
    return ((req, res, next) => {
        let type = JSON.parse(req.params.type)
        if (type.length > 0) {

            let type_sql = ""
            type.map((type_el, index) => {
                type_sql += `${index <= 0 ? "" : ","}"${type_el}"` /// "temp","humid","time"
            })

            db.query(`SELECT * FROM critical_information WHERE critical_information.device_type IN (${type_sql})`, (err, Selectresult) => {
                if (err) throw err
                else {

                    res.result = Selectresult;
                    console.log(res.result);


                }
                next();
            });
        } else {
            res.status(200).json(errorMessage.invalid_data)
        }

    });
}


exports.update_device_activity = () => {
    return ((req, res, next) => {

        // console.log(req.body.device_critical)
        let data = [
            req.body.action,
            JSON.stringify(req.body.device_critical),
            req.body.client_id
        ]
        console.log(data)
        db.query(`UPDATE device_information SET device_action = ? ,device_critical = ? WHERE client_id = ?`, data, (err, result) => {
            if (err) throw err
        });
        next();
    });
}

exports.send_time_setup_to_app = () => {
    return ((req, res, next) => {

        db.query(`SELECT * FROM record_date_time_data WHERE client_id = '${req.params.id}'`, (err, result) => {
            // console.log(result);
            if (err) throw err
            else {
                let data_time = []
                result.map((element) => {
                    data_time.push({
                        id: element.id,
                        user_id: element.user_id,
                        client_id: element.client_id,
                        time_array: JSON.parse(element.device_time)
                    })
                })
                res.result = data_time;
                next();
            }
        });

    });
}

// exports.send_time_setup_to_db = () => {
//     return ((req, res, next) => {

//         console.log(req.body)

//         db.query(` SELECT device_time FROM device_information WHERE client_id = ?`, [req.body.client_id], (err, SelectTimeResult) => {
//             if (err) throw err
//             else {

//                 if (SelectTimeResult[0]) {
//                     // let time_arr = JSON.parse(SelectTimeResult[0].device_time) || []
//                     // time_arr.push(...req.body.device_time)

//                     db.query(` UPDATE device_information SET device_time = ? WHERE  client_id = ? `, [JSON.stringify(req.body.device_time), req.body.client_id], (err, result) => {
//                         if (err) throw err
//                         else {
//                             // solinoid.pub_time(data);
//                             solinoid.pub_time(req.body.device_time, req.body.client_id);
//                             next();

//                         }
//                     });
//                 }
//             }
//         });
//         next();
//     });
// }


exports.send_time_setup_to_db = () => {
    return ((req, res, next) => {

        // console.log( req.user_id)
        let obj = {
            user_id: req.user_id,
            client_id: req.body.client_id,
            device_time: JSON.stringify(req.body.device_time)
        }
        // [{"time":"2020-11-01 22:52:00","action":"on"},{"time":"2020-11-01 22:52:00","action":"off"}]
        db.query(`INSERT INTO record_date_time_data SET ?  `, obj, (err, result) => {
            if (err) throw err
            else {
                // solinoid.pub_time(data);
                console.log();
                solinoid.pub_time(req.body.device_time, req.body.client_id);
                next();

            }
        });
    });

}

