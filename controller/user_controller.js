var io = require('socket.io-client');
var bcrypt = require('bcryptjs');
var db = require('../api/db');
var encrytp = require('../const/encrypt');
var jsonwebToken = require('jsonwebtoken');
var errorMessager = require('../const/error_message');
var constance = require('../const/constance');


exports.user_register = () => {
    return (req, res, next) => {
        // console.log(req.body);

        let data = [
            null,
            req.body.email,
            encrytp.encrytp(req.body.password), // req.body.password,
            req.body.firstname,
            req.body.lastname,
            req.body.address,
            req.body.phonenumber,
            req.body.user_type
        ]


        db.query(`INSERT INTO user_information (users_id,email,password,name,last_name,address,phone_number,user_type) VALUES (?)`, [data], function (err, result) {
            if (err) throw err
            next();
        });

    }
}


exports.User_Login = () => {
    return (req, res, next) => {
        
        let email = req.body.email
        let id = null
        let type = null

        db.query('SELECT * FROM user_information WHERE email = ? ', email, (err, result) => {
            if (err) throw err;//console.log(SELECT * From useraccount WHERE Username = '${Username}')
            if (result[0]) {
                let password = result[0].password
                console.log("data : ",bcrypt.compareSync(req.body.password, password))
                if (bcrypt.compareSync(req.body.password, password)) {
                
                    req.token = jsonwebToken.sign({
                        id: result[0].user_id,
                        type: result[0].user_type
                    }, constance.sign)
                    console.log(req.token)
                    next()
                }
                else {
                    res.status(200).json(errorMessager.err_wrong_password)
                }
            }
            else {
                res.status(200).json(errorMessager.user_work_not_found)
            }
        });
    }
}

exports.facebook_login = () => {
    return (req, res, next) => {
        // console.log(req.body)
        let obj = {
            facebook_id: req.body.facebook_id,
            name: req.body.name,
            email: req.body.email,
            // type_user: 'A'
        }
        db.query('SELECT * FROM user_information WHERE facebook_id=?', obj.facebook_id, (err, result_user) => {
            if (err) throw err
            if (result_user[0]) {
                req.token = jsonwebToken.sign({
                    id: result_user[0].user_id,
                    // type: result_user[0].type_user
                }, constance.sign)

                next()
            }
            else {
                db.query('INSERT INTO user_information SET ?', obj, (err, result) => {
                    if (err) throw err
                    else {
                        req.token = jsonwebToken.sign({
                            id: result.insertId,
                            // type: obj.type_user
                        }, constance.sign)

                        next()
                    }
                })
            }
        })
    }
}

exports.update_facebook_id = () => {
    return (req, res, next) => {
        db.query('UPDATE user_information SET facebook_id=? WHERE user_id=?', [req.body.facebook_id, req.user_id], (err) => {
            if (err) throw err
            else {
                next()
            }
        })
    }
}

exports.user_update_password = () => {
    return (req, res, next) => {
        let user_id = req.user_id

        db.query(`SELECT password FROM user_information WHERE user_id = ?`, user_id, (err, result) => {
            if (err) throw err;//console.log(SELECT * From useraccount WHERE Username = '${Username}')
            if (result[0]) {
                let password = result[0].password
                if (bcrypt.compareSync(req.body.password, password)) {

                    let updateInfo = {
                        user_id: req.user_id,
                        password: encrytp.encrytp(req.body.newpassword)
                    }

                    if (updateInfo.password !== "") {

                        db.query("UPDATE user_information SET password = ? WHERE user_id  = ?", [updateInfo.password, updateInfo.user_id], function (err, result) {
                            if (err) throw err;
                            console.log("okkub")
                            next();
                        })
                    }
                    else {
                        console.log("pass0...", req.body.newpassword)
                    }
                }
                else {
                    res.status(200).json(errorMessager.err_check_password)
                }
            }
            else {
                res.status(200).json(errorMessager.err_user_not_found)
            }
        })
    }
}




exports.user_data = () => {   //ข้อมูล User
    return ((req, res, next) => {
        
        let user_id = req.user_id;

        db.query(`SELECT * FROM user_information WHERE user_id = '${user_id}'`, (err, Selectresult) => {
            if (err) throw err
            else {
                res.result = Selectresult;
            }
            next();
        });
    });
}


exports.get_user_data = () => {   //admin
    return ((req, res, next) => {

        db.query('SELECT * FROM user_information WHERE user_type = 1', (err, Selectresult) => {
            if (err) throw err
            else {
                res.result = Selectresult;
            }
            next();
        });
    });
}

exports.edit_users_data = () => {     //admin
    return ((req, res, next) => {
        

        db.query(`UPDATE user_information 
                SET name = '${req.body.name}',last_name='${req.body.last_name}',address='${req.body.address}',phone_number='${req.body.phone_number}',profile_image = '${req.body.profile}'
                WHERE user_id ='${user_id}'`, (err, Updateresult) => {
            if (err) throw err
            next();
        });
    });
}

exports.edit_user_data = () => {     //แก้ไขข้อมูล User //บัค
    return ((req, res, next) => {
        console.log(req.body);
        let user_id = req.user_id

        db.query(`UPDATE user_information 
                SET name = '${req.body.name}',last_name='${req.body.last_name}',address='${req.body.address}',phone_number='${req.body.phone_number}',profile_image = '${req.body.profile}'
                WHERE user_id ='${user_id}'`, (err, Updateresult) => {
            if (err) throw err
            next();
        });
    });
}

exports.delete_data = () => {  //ลบข้อมูล Users
    return ((req, res, next) => {

        db.query(`DELETE FROM user_information WHERE  user_id = '${req.params.id}'`, (err, result) => {
            if (err) throw err
            next();
        });

    });
}
