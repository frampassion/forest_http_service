var mqtt = require('mqtt')
var moment = require('moment')
const { json } = require('body-parser')
const { decodeBase64 } = require('bcryptjs')
var db = require('../api/db');
var server = require("../const/constance");
var mqtt = require('mqtt')
var client = mqtt.connect(server.mqtt)
var io = require('socket.io-client');
const groupUtil = require("./group_controller");
const con = require('../api/db');
const sht20 = require("./sht20_controller");



exports.solinoid_valve_on = (topic, status) => {
    var client = mqtt.connect(server.mqtt)
    let data = JSON.parse(status)
    let action = data.status

    client.on('connect', () => {
        console.log(topic, status);
        client.publish(topic, status)
        client.end()
    });

}

mqtt_pub_on = () => {
   
    client.on('connect', () => {
        client.publish(client_id, '{"status":"on"}')
        client.end()
        console.log(client_id, '{"status":"on"}');
    });
}

mqtt_pub_off = () => {
    
    client.on('connect', () => {
        client.publish(client_id, '{"status":"off"}')
        client.end()
        console.log(client_id, '{"status":"off"}');
    });
}




exports.pub_time = (time_arr, client_id) => {
    const mqtt = require('mqtt')
    const client = mqtt.connect(server.mqtt)
    const topic = client_id

    let time_test = moment().format('HH:mm')
    console.log(time_test);
    let data = []

    time_arr.map((e) => {
        let hour = moment(e.time).format('H')
        let min = moment(e.time).format('m')
        let sec = moment(e.time).format('s')

        hour = parseInt(hour);
        min = parseInt(min);
        sec = parseInt(sec);

        if (e.time < time_test) {
            // console.log("0")
            status = 0
        }
        else {
            // console.log("1")
            status = 1
        }

        data.push({
            hour: hour,
            min: min,
            sec: sec,
            action: e.action,
            status: status
        });

        console.log(data)
    });

    let time_to_device = data


    client.on('connect', () => {
        client.publish(client_id, JSON.stringify(time_to_device))
        console.log(client_id, JSON.stringify(time_to_device));
        client.end()
    });

}

exports.send_time = () => {
    return ((req, res, next) => {
        // console.log("time : ",time)
        db.query(`SELECT device_time FROM record_date_time_data WHERE client_id = '${req.params.id}'`, (err, result) => {
            if (err) throw err
            else {
                let data = []
                let hour = null
                let min = null
                let sec = null
                let status = null
                let time_array = null
                let time_test = moment().format('HH:mm')


                result.map((e) => {
                    time_array = JSON.parse(e.device_time)

                })

                time_array.map((e_time) => {

                    let hour = moment(e_time.time).format('HH:mm:ss')

                    // console.log(moment(e_time.time).format('HH:mm'))
                    // console.log(time_test)
                    if (e_time.time < time_test) {
                        console.log("0")
                        status = 0
                    }
                    else {
                        console.log("1")
                        status = 1
                    }

                    h = moment(e_time.time).format('H');
                    m = moment(e_time.time).format('m');
                    s = moment(e_time.time).format('s');

                    hour = parseInt(h);
                    min = parseInt(m);
                    sec = parseInt(s);

                    data.push({
                        hour: hour,
                        min: min,
                        sec: sec,
                        action: e_time.action,
                        status: status
                    });

                    console.log(data)
                })

                // let device_critical = null
                // if (result[0]) { //เช็คข้อมูล
                //     device_critical = JSON.parse(result[0].device_critical) //ทำให้เป็น json
                //     // console.log(device_critical)
                // }

                req.result = data

                next();
            }
        });

    });
}


// exports.send_time_setup_to_db = () => {
//     return ((req, res, next) => {

//         db.query(` SELECT device_time FROM device_information WHERE client_id = ?`, [req.body.client_id], (err, SelectTimeResult) => {
//             if (err) throw err
//             else {

//                 if (SelectTimeResult[0]) {
//                     let time_arr = JSON.parse(SelectTimeResult[0].device_time) || []
//                     time_arr.push(...req.body.device_critical)

//                     db.query(` UPDATE device_information SET device_time = ? WHERE  client_id = ? `, [JSON.stringify(time_arr), req.body.client_id], (err, result) => {
//                         if (err) throw err
//                         else {
//                             // solinoid.pub_time(data);
//                             solinoid.pub_time(req.body.device_time, req.body.client_id);
//                             next();

//                         }
//                     });
//                 }
//             }
//         });
//     });
// }

// exports.send_time_setup_to_app = () => {
//     return ((req, res, next) => {

//         db.query(`SELECT device_time FROM device_information WHERE client_id = '${req.params.id}'`, (err, result) => {
//             // console.log(result);
//             if (err) throw err
//             else {
//                 let time_array = []
//                 if (result[0]) {

//                     time_array = JSON.parse(result[0].device_time) || []
//                     res.result = time_array;
//                 }

//                 next();
//             }
//         });

//     });
// }

// exports.test = () => {
//     return ((req, res, next) => {
//         let client_id = req.body.device
//         let group_id = req.body.group_id


//         db.query(`SELECT group_id FROM device_information WHERE client_id = ?`, client_id, (err, Selectreult) => {
//             if (err) throw err
//             else {

//                 let group_id = Selectreult[0].group_id;
//                 // console.log( group_id );

//                 db.query(`SELECT device_type,client_id,device_critical,device_action FROM device_information WHERE group_id = ? AND device_type = 'solenoid_valve'`, group_id, (err, result) => {
//                     if (err) throw err
//                     else {
//                         result.map((e) => {

//                             if (e.device_action == "auto") {
//                                 console.log("true");
//                             }
//                         });
//                         res.result = result;
//                         // console.log(res.result);

//                     }
//                     next();
//                 });
//             }

//         });
//     });
// }

exports.solinoid_valve_auto = (message) => {

    let client_id = message.device
    let temp = parseInt(message.temp)
    let humid = parseInt(message.humid)

    // console.log(temp,humid)
    db.query(`SELECT group_id FROM device_information WHERE client_id = ?`, client_id, (err, Selectreult) => {
        if (err) throw err
        else {

            let group_id = Selectreult[0].group_id;
            // console.log( group_id );

            db.query(`SELECT device_type,client_id,device_critical,device_action FROM device_information WHERE group_id = ? AND device_type = 'solenoid_valve' AND device_action = 'auto'`, group_id, (err, result) => {
                if (err) throw err
                else {

                    // console.log("result.length",result.length)

                    result.map((e) => {

                        let critical = JSON.parse(result[0].device_critical)
                        let max_value = critical.max_value;
                        let min_value = critical.min_value;
                        let option = critical.option;
                        let client_id = e.client_id;

                        // console.log(JSON.parse(result[0].device_critical))
                        // console.log(option);

                        var client = mqtt.connect(server.mqtt)
                        // client.on('connect', () => {
                        //     client.publish(device_id, '{"status":"off"}')
                        //     client.end()
                        //     console.log(device_id, '{"status":"off"}');
                        // });

                        switch (option) {
                            case "temp":
                                console.log(temp, max_value)
                                if (temp <= max_value) {

                                    mqtt_pub_on();

                                    break;
                                }
                                if (temp < min_value) {

                                    mqtt_pub_off(); 

                                    break;
                                }


                            case "humid":
                                console.log(humid, max_value)
                                if (humid <= min_value) {

                                   mqtt_pub_on(); 

                                    break;
                                }
                                if (humid >= max_value) {

                                    mqtt_pub_off(); 

                                    break;
                                }
                        }
                    });
                }
            });
        }
    });

}

exports.solenoid_on_off = () => {
    return ((req, res, next) => {

        let obj = [
            req.body.action,
            req.body.client_id
        ]
        console.log(obj)

        db.query(`UPDATE device_information SET device_action = ? WHERE client_id = ?`, obj, (err, result) => {
            if (err) throw err
            else {
                var client = mqtt.connect(server.mqtt)
                client.on('connect', () => {
                    client.publish(req.body.client_id, `{"status":"${req.body.action}"}`)
                    client.end()

                });

            }
        });
        next();
    });
}


exports.sub_ec_ph_data = () => {
    var mqtt = require("mqtt")
    var client = mqtt.connect(server.mqtt)
    var topic = 'sensor_output'

    client.on('message', (topic, msg) => {
        // console.log(msg); 
        message = msg.toString();
        message_ec_ph = JSON.parse(msg)
        send_ec_ph_data(message_ec_ph);

        console.log(message_ec_ph);

    });
    client.on('connect', () => {
        client.subscribe(topic)
    });
}


send_ec_ph_data = (message) => {

    let json_data = null

    try {
        json_data = message

    } catch (err) {
        json_data = [
            json_EC_data = { EC: '00', TEMP: '00' },
            json_PH_data = { PH: '00' }
        ]
    }
    // console.log(JSON.parse(json_data.payload));
    let payload = {
        name: 'EC_PH_value',
        msg: json_data
    }
    // console.log(payload);
    try {
        var socket = io(server.socket)
        socket.emit('data_tranfer', payload)

    } catch (err) {
        console.log('', err)
    }
}

