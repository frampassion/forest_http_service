var db = require('../api/db');
var errorMessages = require('../const/error_message')
var jsonwebToken = require('jsonwebtoken');
var constance = require('../const/constance');

exports.validate_user_register = () => {
  return ((req, res, next) => {
    console.log(req.body)
    if (req.body.username &&
      req.body.password &&
      req.body.name &&
      req.body.last_name &&
      req.body.address &&
      req.body.phone_number) {
      next();
    }
    else {
      res.status(200).json(errorMessages.invalid_data)
    }
  });
}

exports.validate_user_login = () => {
  return ((req, res, next) => {
    console.log(req.body);
    if (req.body.email &&
      req.body.password) {
      next();
    }
    else {
      res.status(200).json(errorMessages.invalid_data)
    }
  });
}

exports.validate_token_user = function () {
  return function (req, res, next) {
    // console.log(req.session.token)
    if (!Boolean(req.headers["authorization"])) {
      res.status(200).json({
        success: false,
        message: errorMessages.err_required_token
      });
    } else {
      // console.log("token")
      jsonwebToken.verify(
        req.headers.authorization,
        constance.sign,
        (err, decode) => {
          if (err) {
            // console.log(decode.type)
            res.status(200).json(errorMessages.err_required_fingerprint_token);
          } else {
            req.user_id = decode.id;
            next();

          }
        }
      );
    }
  };
};

exports.validate_user_update_password = () => {
  return (req, res, next) => {
    if (req.body.password &&
      req.body.newpassword) {
      next()
    }
    else {
      res.status(200).json(errorMessages.invalid_data)
    }
  }
}

exports.validate_edit_data = () => {
  return ((req, res, next) => {
    console.log(req.body);
    if (req.body.username &&
      req.body.password &&
      req.body.name &&
      req.body.last_name &&
      req.body.address &&
      req.body.phone_number) {
      next();
    }
  });
}

exports.validate_token_user = function () {
  return function (req, res, next) {
    // console.log(req.session.token)
    if (!Boolean(req.headers["authorization"])) {
      res.status(200).json({
        success: false,
        message: errorMessages.err_required_token
      });
    } else {
      console.log("token", req.headers.authorization)
      jsonwebToken.verify(
        req.headers.authorization,
        constance.sign,
        (err, decode) => {
          if (err) {
            // console.log(decode.type)
            res.status(200).json(errorMessages.err_required_fingerprint_token);
          } else {

            // console.log(decode.id)
            req.user_id = decode.id;
            next();

          }
        }
      );
    }
  };
};


// exports.validate_device_activity_data = () => {
//   return ((req, res, next) => {
//     console.log(req.body);
//     if (req.body.device_action&&
//       req.body.device_critical&&
//       req.body.client_id) {
//       next();
//     }
//     else{
//       res.status(200).json(errorMessages.invalid_data)
//     }
//   });
// }
