var bcrypt = require('bcryptjs');
var mqtt = require('mqtt');
var io = require('socket.io-client');
const { json } = require('body-parser');
var moment = require('moment');
var db = require('../api/db');
var server = require("../const/constance");
const e = require('express');
var time_zone = moment().utc(+7).format('DD/MM/YYY HH: mm: ss');
// var canstance = require('../const/constance');
const solinoid = require('./warter_controller');
const errorMessage = require('../const/error_message');




// exports.group_data = () => {  
//     return ((req, res, next) => {
//         db.query('SELECT * FROM group_information', (err, Selectresult) => {
//             if (err) throw err
//             else {
//                 res.result = Selectresult;
//             }
//             next();
//         });
//     });
// }


exports.get_group_data = () => {  
    return ((req, res, next) => {
        // console.log(req.user_id);
        db.query(`SELECT * FROM group_information WHERE user_id = '${req.user_id}'`, (err, Selectresult) => {
           
            if (err) throw err
            else {
                res.result = Selectresult;
            }
            next();
        });
    });
}

exports.insert_group_data = () =>{
    return((req,res,next)=>{
        let user_id =req.user_id
        console.log("req.body : ",req.body)
        // console.log(user_id);
        let insert_data = [
            null,
            req.body.group_name,
            user_id
        ]
        console.log("aom",insert_data)

        db.query(`INSERT INTO group_information (group_id,group_name,user_id) VALUES (?)`, [insert_data], (err,Insertresult)=>{
            if(err) throw err
            next();
        });
    });
}

exports.update_group_data = () =>{
    return((req,res,next)=>{
        console.log(req.body)
        db.query(`UPDATE group_information SET  group_name = '${req.body.group_name}' WHERE group_id ='${req.body.group_id}'`,(err,Updateresult)=>{
            if(err) throw err
            next();
        });

    }); 
}


exports.delete_group_data = () =>{
    return((req,res,next)=>{
        db.query(`DELETE FROM group_information WHERE group_id = '${req.body.group_id}'`,(err,Deleteruslt)=>{
            if(err) throw err
            else{
                res.result = Deleteruslt;
            }
           next(); 
        });
    });
}
